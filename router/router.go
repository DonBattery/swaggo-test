package router

import (
	"fmt"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"gitlab.com/DonBattery/swaggo-test/docs"

	"gitlab.com/DonBattery/swaggo-test/handler"
)


func NewRouter(version string) *gin.Engine {
	router := gin.Default()
	
	// Simple group: v1
	v1 := router.Group("/v1")
	{
		v1.POST("/login", handler.LoginEndpoint)
		v1.POST("/submit", handler.SubmitEndpoint)
		v1.GET("/read", handler.ReadEndpoint)
	}

	// Access the Swagger UI at /swagger/index.html
	// if the App's environment is not production
	if env := strings.ToLower(os.Getenv("ENV")); !strings.HasPrefix(env, "prod") {
		router.GET("/swagger/*any", swaggerHandler(fmt.Sprintf("%s - %s", env, version)))
	}
	
	return router
}

// swaggerHandler sets up the docs.SwaggerInfo according to the environment and constants,
// and returns a ginSwagger.WrapHandler.
func swaggerHandler(version string) gin.HandlerFunc {
	// set Swagger info
	docs.SwaggerInfo.Title = "TestService"
	docs.SwaggerInfo.Version = version
	docs.SwaggerInfo.Description = "TestService's description..."
	return ginSwagger.WrapHandler(swaggerFiles.Handler)
}