package handler

import (
	"log"

	"github.com/gin-gonic/gin"

	"gitlab.com/DonBattery/swaggo-test/model"
)

// LoginEndpoint the Golang dockstring of the function...
// @Summary Log in to the site
// @Description Post a Person's Name and Address to log into the site
// @Tags Auth
// @Accepts json
// @Produce json
// @Param Request-body body model.Person true "Required request body"
// @success 200 {object} model.LoginSuccess "Login Success"
// @failure 400 {object} model.APIError "Missing person Name"
// @failure 500 {object} model.APIError "Login failed"
// @Router /v1/login [post]
func LoginEndpoint(c *gin.Context) {
	var person model.Person
	err := c.ShouldBind(&person)
	if err != nil {
		c.JSON(500, model.APIError{
			StatusCode: 500,
			Error: err.Error(),
		})
		return
	}
	if person.Name == "" {
		c.JSON(400, model.APIError{
			StatusCode: 400,
			Error: "No username provided",
		})
		return
	}
	log.Println(person.Name)
	log.Println(person.Address)
	c.JSON(200, model.LoginSuccess{
		ID: "asdf",
	})
}

// SubmitEndpoint the Golang dockstring of the function...
// @Summary Submit
// @Description Submit a post to the site
// @Tags Service
// @Accepts json
// @Produce json
// @success 200 {object} model.SubmitSuccess "Submit Success"
// @Router /v1/submit [post]
func SubmitEndpoint(c *gin.Context) {
	c.JSON(200, model.SubmitSuccess{
		ID: "asdf",
	})
}

// ReadEndpoint the Golang dockstring of the function...
// @Summary Read
// @Description Read posts from the site
// @Tags Service
// @Produce json
// @success 200 {object} model.ReadSuccess "Read Success"
// @Router /v1/read [get]
func ReadEndpoint(c *gin.Context) {
	c.JSON(200, model.ReadSuccess{
		ID: "asdf",
	})
}
