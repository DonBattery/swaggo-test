package model

type Person struct {
	Name     string    `json:"name" example:"Miki"`
	Address  string    `json:"address"`
}

type APIError struct {
	StatusCode int    `json:"status_code"`
	Error      string `json:"error"`
}

type LoginSuccess struct {
	ID string `json:"id"`
}

type SubmitSuccess struct {
	ID string `json:"id"`
}

type ReadSuccess struct {
	ID string `json:"id"`
}