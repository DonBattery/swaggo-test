package main

import "gitlab.com/DonBattery/swaggo-test/router"

var VERSION string = "latest"

// @termsOfService https://http.cat
// @contact.name Miklos Lorinczi(DevOps Engineer)
// @contact.email m.lorinczi@xximo.com
func main() {
	r := router.NewRouter(VERSION)

	r.Run() // listen and serve on 0.0.0.0:8080
}
